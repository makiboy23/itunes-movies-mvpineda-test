//
//  Track.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

class Track: NSObject, Decodable, NSCoding {
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(trackName ?? "", forKey: "trackNameKey")
        aCoder.encode(genre ?? "", forKey: "primaryGenreNameKey")
        aCoder.encode(artworkUrl100 ?? "", forKey: "artworkKey")
        aCoder.encode(longDescription ?? "", forKey: "longDescripKey")
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.trackName = aDecoder.decodeObject(forKey: "trackNameKey") as? String
        self.genre = aDecoder.decodeObject(forKey: "primaryGenreNameKey") as? String
        self.artworkUrl100 = aDecoder.decodeObject(forKey: "artworkKey") as? String
        self.longDescription = aDecoder.decodeObject(forKey: "longDescripKey") as? String
    }
    
    required override init() {}
    
    var artistId: Int?
    var artistName: String?
    
    var trackId: Int?
    var trackName: String?
    var trackPrice: Double?
    var genre: String?
    
    var currency: String?
    
    var artworkUrl600: String?
    var artworkUrl100: String?
    var artworkUrl60: String?
    var artworkUrl30: String?
    
    var longDescription: String?
}
