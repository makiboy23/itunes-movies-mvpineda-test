//
//  UserDefaultsHelper.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//


import Foundation

extension UserDefaults {
  
    static let favoritesTrackKey = "favoritesTrackKey"
    
    func addTrack(track: Track) {
        var savedTracksData = UserDefaults.standard.savedTracks()
        
        let hasFavorited = savedTracksData.firstIndex(where: { $0.trackName == track.trackName && $0.genre == track.genre }) != nil
        
        if hasFavorited {
            // already saved
        } else {
            // not save
            savedTracksData.append(track)
            let data = NSKeyedArchiver.archivedData(withRootObject: savedTracksData)
            UserDefaults.standard.set(data, forKey: UserDefaults.favoritesTrackKey)
        }
    }
    
    // TODO:- SAVED TRACK
    func savedTracks() -> [Track] {
        guard let savedTracksData = UserDefaults.standard.data(forKey: UserDefaults.favoritesTrackKey) else { return [] }
        guard let savedTracks = NSKeyedUnarchiver.unarchiveObject(with: savedTracksData) as? [Track] else { return [] }
        return savedTracks
    }

    // TODO:- DELETE TRACK
    func deleteTrack(track: Track) {
        let tracks = savedTracks()
        let filteredTracks = tracks.filter { (m) -> Bool in
          return m.trackName != track.trackName && m.genre != track.genre
        }
        let data = NSKeyedArchiver.archivedData(withRootObject: filteredTracks)
        UserDefaults.standard.set(data, forKey: UserDefaults.favoritesTrackKey)
    }
}
