//
//  DateTimeHelper.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

// TODO:- GET CURRENT DATE AND TIME AND DO FORMATTING
func getCurrentDateTime() -> String {
    // get the current date and time
    let currentDateTime = Date()
    
    // initialize the date formatter and set the style
    let formatter = DateFormatter()
    formatter.timeStyle = .medium
    formatter.dateStyle = .long
    
    // get the date time String from the date object
    let formattedDateTime = formatter.string(from: currentDateTime) // October 8, 2016 at 10:48:53 PM
    
    return formattedDateTime
}

// TODO:- SAVE user previously visited
func saveVisitedDateTime() -> String {
    let userDefault = UserDefaults.standard
    let keyName = "PreviouslyVisitedDateTime"
    
    let previouslyVisitedDateTime = getCurrentDateTime()
    userDefault.set(previouslyVisitedDateTime, forKey: keyName)
    
    return previouslyVisitedDateTime
}

// TODO:- FETCH user previously visited
func fetchPreviouslyVisitedDateTime() -> String {
    var dateTime = ""
    
    if let string = UserDefaults.standard.string(forKey: "PreviouslyVisitedDateTime") {
        dateTime = string
    } else {
        // IF NO DATE TIME SAVED
        dateTime = saveVisitedDateTime()
    }
    
    return dateTime
}
