//
//  APIServices.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation

class APIService {
    
    let iTunesAPIBaseUrl = "https://itunes.apple.com"
    
    static let shared = APIService()

    /**
     Load Tracks from iTUNES Search API
     link: https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
     source: https://itunes.apple.com/search?term=star&country=au&media=movie
     method: GET
     */
    func fetchTracksData(completion: @escaping ([Track]) -> ()) {
        let urlString = "\(iTunesAPIBaseUrl)/search?term=star&country=au&media=movie"
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            if let err = err {
                print("Failed to fetch tracks:", err)
                return
            }
            
            guard let data = data else { return }
            do {
                // let tracks = try JSONDecoder().decode([Track].self, from: data)
                
                var tracks = [Track]()
                
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any],
                    let results = json["results"] as? [[String:Any]] {
                    
                    // DO LOOP the results
                    for dict in results as [[String: AnyObject]] {
                        var track = Track()
                        
                        // MUST CHECK FIRST THE WRAPPER TYPE
                        guard let wrapperType = dict["wrapperType"] as? String else { continue }
                        
                        if wrapperType != "track" {
                            // IF NOT TRACK THEN SKIP THE CURRENT LOOP TO CONTINUE
                            continue
                        }
                        
                        track = parseTrack(dict: dict)
                        
                        tracks.append(track)
                    }
                }
                
                DispatchQueue.main.async {
                    completion(tracks)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
        }.resume()
    }
}














