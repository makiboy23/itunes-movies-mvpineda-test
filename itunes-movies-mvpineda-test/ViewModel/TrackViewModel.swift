//
//  TrackViewModel.swift
//  itunes-movies-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import Foundation
import UIKit

struct TrackViewModel {
    var artistName: String
    
    var trackName: String
    var price: String
    var genre: String
    
    var artworkUrl: String
    
    var longDescription: String
    
    // Dependency Injection (DI)
    init(track: Track) {
        if let trackName = track.trackName {
            self.trackName = trackName
        } else {
            self.trackName = ""
        }
        
        if let artistName = track.artistName {
            self.artistName = artistName
        } else {
            self.artistName = ""
        }
        
        if let trackPrice = track.trackPrice,
            let currency = track.currency {
            self.price = "\(trackPrice) \(currency)"
        } else {
            self.price = ""
        }
        
        if let genre = track.genre {
            self.genre = genre
        } else {
            self.genre = ""
        }
        
        if let artworkUrl = track.artworkUrl600 {
            self.artworkUrl = artworkUrl
        } else {
            self.artworkUrl = ""
        }
        
        if let longDescription = track.longDescription {
            self.longDescription = longDescription
        } else {
            self.longDescription = ""
        }
    }
}
