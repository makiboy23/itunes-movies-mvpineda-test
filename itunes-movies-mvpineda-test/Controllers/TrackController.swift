//
//  TrackController.swift
//  itunes-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

class TrackController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    var trackViewModel: TrackViewModel! {
        didSet {
            coverImageView.loadImage(urlString: trackViewModel.artworkUrl)
            trackNameLabel.text = trackViewModel.trackName
            artistNameLabel.text = trackViewModel.artistName
            priceLabel.text = trackViewModel.price
            longDescriptionLabel.text = trackViewModel.longDescription
            genreLabel.text = trackViewModel.genre
        }
    }
    
    let stackView: UIStackView = {
        let sv = UIStackView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        return sv
    }()
    
    let coverImageView: CachedImageView = {
        let iv = CachedImageView()
        iv.backgroundColor = .black
        iv.contentMode = .scaleAspectFit
        iv.translatesAutoresizingMaskIntoConstraints = false
        return iv
    }()
    
    let trackNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textColor = .black
        lbl.textAlignment = .left
        lbl.numberOfLines = -1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let artistNameLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 11)
        lbl.textColor = .gray
        lbl.textAlignment = .left
        lbl.numberOfLines = -1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let longDescriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textColor = .black
        lbl.textAlignment = .left
        lbl.numberOfLines = -1
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let priceLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textColor = .orange
        lbl.textAlignment = .right
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreLabel: UILabel = {
        let lbl = UILabel()
        lbl.font = UIFont.systemFont(ofSize: 14)
        lbl.textColor = .white
        lbl.textAlignment = .center
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    let genreBadgeView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 31/255, green: 32/255, blue: 34/255, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 6
        view.layer.masksToBounds = true
        return view
    }()
    
    lazy var dismissButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("DISMISS", for: .normal)
        btn.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        btn.translatesAutoresizingMaskIntoConstraints = false
        return btn
    }()
    
    private func setupViews() {
        view.backgroundColor = .white
        
        view.addSubview(coverImageView)
        coverImageView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        coverImageView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        coverImageView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        coverImageView.heightAnchor.constraint(equalTo: coverImageView.widthAnchor, multiplier: 1.0).isActive = true
        
        view.addSubview(priceLabel)
        priceLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        priceLabel.topAnchor.constraint(equalTo: coverImageView.bottomAnchor, constant: 10).isActive = true
        
        view.addSubview(trackNameLabel)
        trackNameLabel.topAnchor.constraint(equalTo: coverImageView.bottomAnchor, constant: 10).isActive = true
        trackNameLabel.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        trackNameLabel.rightAnchor.constraint(equalTo: priceLabel.leftAnchor, constant: 10).isActive = true
        
        view.addSubview(artistNameLabel)
        artistNameLabel.topAnchor.constraint(equalTo: trackNameLabel.bottomAnchor).isActive = true
        artistNameLabel.leftAnchor.constraint(equalTo: trackNameLabel.leftAnchor).isActive = true
        artistNameLabel.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        
        view.addSubview(longDescriptionLabel)
        longDescriptionLabel.topAnchor.constraint(equalTo: artistNameLabel.bottomAnchor, constant: 14).isActive = true
        longDescriptionLabel.leftAnchor.constraint(equalTo: artistNameLabel.leftAnchor).isActive = true
        longDescriptionLabel.rightAnchor.constraint(equalTo: artistNameLabel.rightAnchor).isActive = true
        
        view.addSubview(genreLabel)
        genreLabel.topAnchor.constraint(equalTo: coverImageView.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        genreLabel.rightAnchor.constraint(equalTo: coverImageView.rightAnchor, constant: -20).isActive = true
        
        view.insertSubview(genreBadgeView, belowSubview: genreLabel)
        genreBadgeView.topAnchor.constraint(equalTo: genreLabel.topAnchor, constant: -10).isActive = true
        genreBadgeView.leftAnchor.constraint(equalTo: genreLabel.leftAnchor, constant: -10).isActive = true
        genreBadgeView.rightAnchor.constraint(equalTo: genreLabel.rightAnchor, constant: 10).isActive = true
        genreBadgeView.bottomAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 10).isActive = true
        
        view.addSubview(dismissButton)
        dismissButton.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        dismissButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
    }
    
    // ACTIONS
    
    @objc func handleDismiss() {
        self.dismiss(animated: true)
    }
    
    // MAKE THE STATUS TEXT WHITE
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
