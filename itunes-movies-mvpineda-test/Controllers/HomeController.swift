//
//  ViewController.swift
//  itunes-movies-mvpineda-test
//
//  Created by Marknel Pogi on 5/17/19.
//  Copyright © 2019 marknelpogi. All rights reserved.
//

import UIKit

protocol HomeControllerProtocol {
    func navigateToTrackController(trackViewModel: TrackViewModel)
}

class HomeController: UICollectionViewController, HomeControllerProtocol {

    override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
    }
    
    var trackViewModels = [TrackViewModel]() {
        didSet {
            collectionView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        fetchMovies()
        setupObservers()
    }
    
    var previouslyVisitedDateTime = ""
    
    let cellId = "cellId"
    let cellHeaderId = "cellHeaderId"
    let cellMoviesId = "cellMoviesId"
    
    // SETUP VIEWS
    private func setupViews() {
        if #available(iOS 11.0, *) {
            collectionView?.contentInsetAdjustmentBehavior = .never
        }
        
        // FETCH PREVIOUSLY DATE TIME
        previouslyVisitedDateTime = fetchPreviouslyVisitedDateTime()
        
        collectionView.backgroundColor = .white
        
        collectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.register(HeaderListCell.self, forCellWithReuseIdentifier: cellHeaderId)
        collectionView.register(MoviesCell.self, forCellWithReuseIdentifier: cellMoviesId)
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func setupObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK:- NAVIGATION
extension HomeController {
    func navigateToTrackController(trackViewModel: TrackViewModel) {
        let controller = TrackController()
        controller.trackViewModel = trackViewModel
        present(controller, animated: true)
    }
}

// MARK:- OBSERVERS
extension HomeController {
    @objc func willResignActive() {
        // TODO:- Do save/update current date and time when the app is resign active
        
        previouslyVisitedDateTime = saveVisitedDateTime()
        collectionView.reloadData()
    }
}

// MARK:- FETCH DATA
extension HomeController {
    private func fetchMovies() {
        APIService.shared.fetchTracksData { (tracks: [Track])  in
            self.trackViewModels = tracks.map({return TrackViewModel(track: $0)})
            
            if tracks.count != 0 {
                // SAVED ALL TRACK
               let _ = tracks.map({UserDefaults.standard.addTrack(track: $0)})
            } else {
                // LOAD SAVED TRACKS WHEN OFFLINE and NO TRACKS FOUND
                let savedTracks = UserDefaults.standard.savedTracks()
                self.trackViewModels = savedTracks.map({return TrackViewModel(track: $0)})
            }
        }
    }
}

// MARK: COLLECTIONVIEW
extension HomeController: UICollectionViewDelegateFlowLayout {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellHeaderId, for: indexPath) as! HeaderListCell
            cell.previouslyVisitedDateTime = previouslyVisitedDateTime
            return cell
        } else if indexPath.row == 1 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellMoviesId, for: indexPath) as! MoviesCell
            cell.homeControllerProtocol = self
            cell.trackViewModels = trackViewModels
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath)
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var rowSize: CGSize = .zero
        
        // to achieve RESPONSIVE DESIGN CALCULATION
        
        if indexPath.row == 0 {
            rowSize = CGSize(width: view.frame.width, height: 120)
        } else if indexPath.row == 1 {
            // New Music Box Height
            let moviesTitleHeight: CGFloat = 20
            let moviesBoxSpacingBetween: CGFloat = 10
            let moviesRowMargin: CGFloat = 40
            let rowSectionBottomSpacing: CGFloat = 40
            
            // Calculate 2 boxes per row with 10 spacing between
            let moviesBoxHeight: CGFloat = (view.frame.width - (moviesBoxSpacingBetween + moviesRowMargin)) / 2
            
            let moviesRowCount: CGFloat = ceil(CGFloat(trackViewModels.count / 2))
            let moviesRowSpacing: CGFloat = moviesRowCount * 10
            
            let moviesSectionHeight = (moviesBoxHeight * moviesRowCount + moviesRowSpacing) + rowSectionBottomSpacing
            
            // 6x = 6x Box
            let moviesRowSize = CGSize(width: view.frame.width, height: moviesSectionHeight + moviesTitleHeight + 20)
            
            rowSize = moviesRowSize
        }
        
        return rowSize
    }
}
