# iTunes Movies by Marknel Pineda

Appetiser Mobile Coding Challenge

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

![](https://marknelpogi.com/assets/itunes-movies.gif)

## INTRODUCTION
iTunes Movies by Marknel Pineda  is an iOS application that display Movies. By using iTunes Search API.

### Requirements
```
iOS 11.0+
Xcode 10.2+
Swift Latest
```

## API
```
iTunes Search API - https://affiliate.itunes.apple.com/resources/documentation/itunes-store-web-service-search-api/
```

## Project information
For this project I used the MVVM architecture pattern and Userdefaults to save current tracks and load it when the internet is offline or no tracks is loaded. Also I used Userdefault to save "A date when user previously visited, shown in the list header." the saving will triggered when the user resign active(minimize) the app and the app will terminated.The display will show on "Top Right" of the screen. I also used CachedImageView Class to handle the image caching. The UI of the application is responsive to all iPhones and iPads but best viewed on iPhones. Coding Approach is Programmatically one of my favorite on this appoarch is I can easily manage the constraints of the view.


## PERSISTENCE
USERDEFAULT 

- The application is capable to load previous tracks list when the internet is offline or no tracks is loaded.

The Helper is located at  

- Helper > DateTimeHelper.swift
- Helper > UserDefaultsHelper.swift

## IMAGE CACHE
The Class is located at:
- Classes > CachedImageView.swift

## SCREEN RESPONSIVENESS 
- The UI of the application is responsive to all iPhones and iPads but best viewed on iPhones

## NETWORK
- URLSession dataTask
I used this URLSession for this project because I only use specific API call. But I can also know how to used Alamofire.

## CODING STYLE
- Programmatic Approach 
